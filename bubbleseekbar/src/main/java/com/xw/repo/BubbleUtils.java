package com.xw.repo;


import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

class BubbleUtils {



     static int sp2px(Context context, int sp) {
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return  (int)(sp * displayAttributes.scalDensity);
    }
     static DisplayAttributes getScreenPiex(Context context){
        // 获取屏幕密度
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        //displayAttributes.xDpi;
        //displayAttributes.yDpi
        return  displayAttributes;
    }

     static int dp2px(Context context, int dp){
        // 获取屏幕密度
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return  (int) (dp * displayAttributes.scalDensity);
    }

    static int getAttrSetInt(AttrSet attrs,String key, int def){
        if (attrs.getAttr(key).isPresent()){
            return attrs.getAttr(key).get().getIntegerValue();
        }else {
            return def;
        }
    }
    static float getAttrSetFloat(AttrSet attrs,String key, float def){
        if (attrs.getAttr(key).isPresent()){
            return attrs.getAttr(key).get().getFloatValue();
        }else {
            return def;
        }
    }
    static boolean getAttrSetBoolean(AttrSet attrs,String key, boolean def){
        if (attrs.getAttr(key).isPresent()){
            return attrs.getAttr(key).get().getBoolValue();
        }else {
            return def;
        }
    }
    static Color getAttrSetColor(AttrSet attrs,String key, Color color){
        if (attrs.getAttr(key).isPresent()){
            return attrs.getAttr(key).get().getColorValue();
        }else {
            return color;
        }
    }
    static String getAttrSetString(AttrSet attrs,String key, String str){
        if (attrs.getAttr(key).isPresent()){
            return attrs.getAttr(key).get().getStringValue();
        }else {
            return str;
        }
    }
}