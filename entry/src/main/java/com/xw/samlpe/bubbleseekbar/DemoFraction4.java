package com.xw.samlpe.bubbleseekbar;

import com.xw.repo.BubbleSeekBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.util.HashMap;
import java.util.Locale;

public class DemoFraction4 extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component parse = scatter.parse(ResourceTable.Layout_fragment_demo_4, container, false);
        ObservableScrollView mObsScrollView =(ObservableScrollView) parse.findComponentById(ResourceTable.Id_demo_4_obs_scroll_view);
        final BubbleSeekBar bubbleSeekBar1 =(BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_4_seek_bar_1);
        final BubbleSeekBar bubbleSeekBar2 =(BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_4_seek_bar_2);
        final BubbleSeekBar bubbleSeekBar3 =(BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_4_seek_bar_3);
        final BubbleSeekBar bubbleSeekBar4 =(BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_4_seek_bar_4);
        final Text progressText1 =(Text) parse.findComponentById(ResourceTable.Id_demo_4_progress_text_1);
        final Text progressText2 =(Text) parse.findComponentById(ResourceTable.Id_demo_4_progress_text_2);
        final Text progressText3 =(Text) parse.findComponentById(ResourceTable.Id_demo_4_progress_text_3);

        mObsScrollView.setOnScrollChangedListener(new ObservableScrollView.OnScrollChangedListener() {
            @Override
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
                bubbleSeekBar1.correctOffsetWhenContainerOnScrolling();
            }
        });
        bubbleSeekBar2.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListenerAdapter() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                String s = String.format(Locale.CHINA, "onChanged int:%d, float:%.1f", progress, progressFloat);
                progressText1.setText(s);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                String s = String.format(Locale.CHINA, "onActionUp int:%d, float:%.1f", progress, progressFloat);
                progressText2.setText(s);
            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                String s = String.format(Locale.CHINA, "onFinally int:%d, float:%.1f", progress, progressFloat);
                progressText3.setText(s);
            }
        });

        // trigger by set progress or seek by finger
        bubbleSeekBar3.setProgress(bubbleSeekBar3.getMax());

        // customize section texts
        bubbleSeekBar4.setCustomSectionTextArray(new BubbleSeekBar.CustomSectionTextArray() {

            @Override
            public HashMap<Integer, String> onCustomize(int sectionCount, HashMap<Integer, String> array) {
                array.clear();
                array.put(1, "bad");
                array.put(4, "ok");
                array.put(7, "good");
                array.put(9, "great");
                return array;
            }
        });
        bubbleSeekBar4.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListenerAdapter() {

            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                try {
                    int color;
                    if (progress <= 10) {
                        color = getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor();
                    } else if (progress <= 40) {
                        color = getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red_light).getColor();
                    } else if (progress <= 70) {
                        color = getFractionAbility().getResourceManager().getElement(ResourceTable.Color_colorAccent).getColor();
                    } else if (progress <= 90) {
                        color = getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_blue).getColor();
                    } else {
                        color = getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_green).getColor();
                    }
                    bubbleSeekBar.setSecondTrackColor(color);
                    bubbleSeekBar.setThumbColor(color);
                    bubbleSeekBar.setBubbleColor(color);
                }catch (Exception e){}
            }
        });
        bubbleSeekBar4.setProgress(60);
        return  parse;
    }
}
