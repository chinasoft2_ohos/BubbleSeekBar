package com.xw.samlpe.bubbleseekbar;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class DemoFraction2 extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return  scatter.parse(ResourceTable.Layout_fragment_demo_2,container,false);
    }
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }
}
