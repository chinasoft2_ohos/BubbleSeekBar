package com.xw.samlpe.bubbleseekbar.slice;

import com.xw.samlpe.bubbleseekbar.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.RadioButton;
import ohos.agp.components.RadioContainer;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        DemoFraction1 demoFraction1 = new DemoFraction1();
        DemoFraction2 demoFraction2 = new DemoFraction2();
        DemoFraction3 demoFraction3 = new DemoFraction3();
        DemoFraction4 demoFraction4 = new DemoFraction4();
        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler()
                .add(ResourceTable.Id_container, demoFraction1)
                .submit();
        RadioContainer rc = (RadioContainer) findComponentById(ResourceTable.Id_rc);
        RadioButton demo1 = (RadioButton) findComponentById(ResourceTable.Id_main_tab_btn_1);
        RadioButton demo2 = (RadioButton) findComponentById(ResourceTable.Id_main_tab_btn_2);
        RadioButton demo3 = (RadioButton) findComponentById(ResourceTable.Id_main_tab_btn_3);
        RadioButton demo4 = (RadioButton) findComponentById(ResourceTable.Id_main_tab_btn_4);
        demo1.setButtonElement(null);
        demo2.setButtonElement(null);
        demo3.setButtonElement(null);
        demo4.setButtonElement(null);
        int color = 0;
        try {
             color = getResourceManager().getElement(ResourceTable.Color_colorAccent).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        int finalColor = color;

        rc.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                demo1.setTextColor(Color.BLACK);
                demo2.setTextColor(Color.BLACK);
                demo3.setTextColor(Color.BLACK);
                demo4.setTextColor(Color.BLACK);
                switch (i){
                    case 0:
                        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler()
                                .replace(ResourceTable.Id_container, demoFraction1)
                                .submit();
                        break;
                    case 1:
                        demo2.setTextColor(new Color(finalColor));
                        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler()
                                .replace(ResourceTable.Id_container, demoFraction2)
                                .submit();
                        break;
                    case 2:
                        demo3.setTextColor(new Color(finalColor));
                        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler()
                                .replace(ResourceTable.Id_container, demoFraction3)
                                .submit();
                        break;
                    case 3:
                        demo4.setTextColor(new Color(finalColor));
                        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler()
                                .replace(ResourceTable.Id_container, demoFraction4)
                                .submit();
                        break;
                }
            }
        });
        rc.mark(0);
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
