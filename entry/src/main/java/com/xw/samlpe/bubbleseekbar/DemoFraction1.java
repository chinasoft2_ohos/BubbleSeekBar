package com.xw.samlpe.bubbleseekbar;

import com.xw.repo.BubbleSeekBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.security.SecureRandom;

public class DemoFraction1 extends Fraction {
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private Text msgToast;
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component parse = scatter.parse(ResourceTable.Layout_fragment_demo_1, container, false);
        BubbleSeekBar bubbleSeekBar = (BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_1_seek_bar);
        bubbleSeekBar.setProgress(20);
        msgToast = (Text) parse.findComponentById(ResourceTable.Id_msgToast);
        parse.findComponentById(ResourceTable.Id_demo_1_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                int progress = new SecureRandom().nextInt((int) bubbleSeekBar.getMax());
                bubbleSeekBar.setProgress(progress);
                onBarClicked("set random progress = " + progress);
            }
        });

        return  parse;
    }

    /**
     * onBarClicked
     *
     * @param ss String
     */
    public void onBarClicked(String ss) {
        msgToast.setText(ss);
        msgToast.setVisibility(Component.VISIBLE);
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                msgToast.setVisibility(Component.HIDE);
            }
        },2000);
    }
}
