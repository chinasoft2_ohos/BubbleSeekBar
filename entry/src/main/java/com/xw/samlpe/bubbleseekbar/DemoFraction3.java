package com.xw.samlpe.bubbleseekbar;

import com.xw.repo.BubbleSeekBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class DemoFraction3 extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component parse = scatter.parse(ResourceTable.Layout_fragment_demo_3, container, false);
        BubbleSeekBar bubbleSeekBar1 = (BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_3_seek_bar_1);
        BubbleSeekBar bubbleSeekBar2 = (BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_3_seek_bar_2);
        BubbleSeekBar bubbleSeekBar3 = (BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_3_seek_bar_3);
        BubbleSeekBar bubbleSeekBar4 = (BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_3_seek_bar_4);
        BubbleSeekBar bubbleSeekBar5 = (BubbleSeekBar) parse.findComponentById(ResourceTable.Id_demo_3_seek_bar_5);
        try {
            bubbleSeekBar1.getConfigBuilder()
                    .min(0)
                    .max(5)
                    .progress(2)
                    .sectionCount(5)
                    .trackColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_gray).getColor())
                    .secondTrackColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_blue).getColor())
                    .thumbColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_blue).getColor())
                    .showSectionText()
                    .sectionTextColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_colorPrimary).getColor())
                    .sectionTextSize(18)
                    .showThumbText()
                    .thumbTextColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor())
                    .thumbTextSize(18)
                    .bubbleColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor())
                    .bubbleTextSize(18)
                    .showSectionMark()
                    .seekStepSection()
                    .touchToSeek()
                    .sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
                    .build();
        } catch (Exception e) {
            System.out.println("======bubbleSeekBar1===" + e.getMessage());
        }
        try {
            bubbleSeekBar2.getConfigBuilder()
                    .min(0)
                    .max(5)
                    .progress(2)
                    .sectionCount(5)
                    .trackColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_gray).getColor())
                    .secondTrackColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_blue).getColor())
                    .thumbColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_blue).getColor())
                    .showSectionText()
                    .sectionTextColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_colorPrimary).getColor())
                    .sectionTextSize(18)
                    .showThumbText()
                    .touchToSeek()
                    .thumbTextColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor())
                    .thumbTextSize(18)
                    .bubbleColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor())
                    .bubbleTextSize(18)
                    .showSectionMark()
                    .seekBySection()
                    .autoAdjustSectionMark()
                    .sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
                    .build();
        } catch (Exception e) {
            System.out.println("======bubbleSeekBar2===" + e.getMessage());
        }
        try {
            bubbleSeekBar3.getConfigBuilder()
                    .min(-50)
                    .max(50)
                    .sectionCount(10)
                    .sectionTextInterval(2)
                    .trackColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red_light).getColor())
                    .secondTrackColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor())
                    .seekBySection()
                    .showSectionText()
                    .sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
                    .build();
        } catch (Exception e) {
            System.out.println("======bubbleSeekBar3===" + e.getMessage());
        }
        try {
            bubbleSeekBar4.getConfigBuilder()
                    .min(1)
                    .max(1.5f)
                    .floatType()
                    .sectionCount(10)
                    .secondTrackColor( getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_green).getColor())
                    .showSectionText()
                    .showThumbText()
                    .build();
        } catch (Exception e) {
            System.out.println("======bubbleSeekBar4===" + e.getMessage());
        }
        try {
            bubbleSeekBar5.getConfigBuilder()
                    .min(-0.4f)
                    .max(0.4f)
                    .progress(0)
                    .floatType()
                    .sectionCount(10)
                    .sectionTextInterval(2)
                    .showSectionText()
                    .sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
                    .bubbleColor(getFractionAbility().getResourceManager().getElement(ResourceTable.Color_color_red).getColor())
                    .autoAdjustSectionMark()
                    .build();
        } catch (Exception e) {
            System.out.println("======bubbleSeekBar5===" + e.getMessage());
        }
        return parse;
    }

}
