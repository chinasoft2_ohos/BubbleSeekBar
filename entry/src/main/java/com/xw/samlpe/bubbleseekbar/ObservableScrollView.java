package com.xw.samlpe.bubbleseekbar;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.app.Context;

public class ObservableScrollView extends ScrollView implements Component.ScrolledListener {

    private OnScrollChangedListener mOnScrollChangedListener;

    public ObservableScrollView(Context context) {
        super(context);
    }

    public ObservableScrollView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    public ObservableScrollView(Context context, AttrSet attrs) {
        super(context, attrs);
    }
    public void setOnScrollChangedListener(OnScrollChangedListener onScrollChangedListener) {
        this.mOnScrollChangedListener = onScrollChangedListener;
    }


    @Override
    public void onContentScrolled(Component component, int x, int y, int oldx, int oldy) {
        if (mOnScrollChangedListener != null) {
            mOnScrollChangedListener.onScrollChanged(this, x, y, oldx, oldy);
        }
    }

    interface OnScrollChangedListener {

        void onScrollChanged(ObservableScrollView scrollView,int x, int y, int oldx, int oldy);

    }
}