/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package com.xw.samlpe.bubbleseekbar;

import com.xw.repo.BubbleSeekBar;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {

    private static BubbleSeekBar bubbleSeekBar;

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.xw.samlpe.bubbleseekbar", actualBundleName);
    }

    @BeforeClass
    public static void set() {
        Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        bubbleSeekBar = new BubbleSeekBar(ability.getContext(), new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        });
        bubbleSeekBar.getConfigBuilder()
                .min(0)
                .max(5)
                .progress(2)
                .sectionCount(5)
                .touchToSeek()
                .showProgressInFloat()
                .seekStepSection()
                .build();
    }

    @Test
    public void getMin() {
        assertTrue(0 == bubbleSeekBar.getConfigBuilder().getMin());
    }

    @Test
    public void getMax() {
        assertTrue(5 == bubbleSeekBar.getConfigBuilder().getMax());
    }

    @Test
    public void get() {
        assertTrue(2 == bubbleSeekBar.getConfigBuilder().getProgress());
    }

    @Test
    public void getSectionCount() {
        assertTrue(5 == bubbleSeekBar.getConfigBuilder().getSectionCount());
    }

    @Test
    public void isTouchToSeek() {
        assertTrue(bubbleSeekBar.getConfigBuilder().isTouchToSeek());
    }

    @Test
    public void isShowProgressInFloat() {
        assertTrue(bubbleSeekBar.getConfigBuilder().isShowProgressInFloat());
    }

    @Test
    public void isSeekStepSection() {
        assertTrue(bubbleSeekBar.getConfigBuilder().isSeekStepSection());
    }

}