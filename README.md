﻿# BubbleSeekBar


#### 项目介绍
- 项目名称：BubbleSeekBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：自定义SeekBar，进度变化由可视化气泡样式呈现，定制化程度较高，适合大部分需求
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本： Release  3.20


#### 效果演示
<img src="img/1.gif"></img>
<img src="img/2.gif"></img>
<img src="img/3.PNG"></img>
<img src="img/4.PNG"></img>
#### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:BubbleSeekBar:1.0.1')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
 Init in xml
```xml
<com.xw.repo.BubbleSeekBar
     ohos:width="match_parent"
     ohos:height="match_content"
    app:bsb_bubble_color="$color:color_red_light"
    app:bsb_bubble_text_color="$color:colorPrimaryDark"
    app:bsb_max="50.0"
    app:bsb_min="-50"
    app:bsb_progress="0"
    app:bsb_second_track_color="$color:color_red"
    app:bsb_section_count="5"
    app:bsb_section_text_position="bottom_sides"
    app:bsb_show_progress_in_float="true"
    app:bsb_show_section_mark="true"
    app:bsb_show_section_text="true"
    app:bsb_show_thumb_text="true"
    app:bsb_track_color="$color:color_red_light"/>
```
```xml
<com.xw.repo.BubbleSeekBar
    ohos:width="match_parent"
    ohos:height="match_content"
    app:bsb_auto_adjust_section_mark="true"
    app:bsb_second_track_color="$color:color_blue"
    app:bsb_section_count="5"
    app:bsb_section_text_position="below_section_mark"
    app:bsb_show_section_mark="true"
    app:bsb_show_section_text="true"
    app:bsb_show_thumb_text="true"
    app:bsb_thumb_text_size="18sp"
    app:bsb_touch_to_seek="true"/>
```
 Init in java (not for **_lite_** version)
```java
mBbubbleSeekBar.getConfigBuilder()
               .min(0.0)
               .max(50)
               .progress(20)
               .sectionCount(5)
               .trackColor(ContextCompat.getColor(getContext(), R.color.color_gray))
               .secondTrackColor(ContextCompat.getColor(getContext(), R.color.color_blue))
               .thumbColor(ContextCompat.getColor(getContext(), R.color.color_blue))
               .showSectionText()
               .sectionTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
               .sectionTextSize(18)
               .showThumbText()
               .thumbTextColor(ContextCompat.getColor(getContext(), R.color.color_red))
               .thumbTextSize(18)
               .bubbleColor(ContextCompat.getColor(getContext(), R.color.color_green))
               .bubbleTextSize(18)
               .showSectionMark()
               .seekBySection()
               .autoAdjustSectionMark()
               .sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
               .build();
```
查看demo获知更多使用细节
- 当自定义section texts的时候，你首先需要确保属性 `bsb_section_text_position` 已经被设置为 `below_section_mark`，
然后在java代码中参照以下代码实现你自己的需求：
```java
   mBubbleSeekBar.setCustomSectionTextArray(new BubbleSeekBar.CustomSectionTextArray() {
       @NonNull
       @Override
       public HashMap<Integer, String> onCustomize(int sectionCount, @NonNull SparseArray<String> array) {
           array.clear();
           array.put(1, "bad");
           array.put(4, "ok");
           array.put(7, "good");
           array.put(9, "great");

           return array;
       }
   });
```



#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.1

#### 版权信息
```
   Copyright 2017 woxingxiao

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```